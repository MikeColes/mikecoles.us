---
title: "Projects"
sitemap:
  priority : 0.9
---
This section contains projects [created](#creations) and [contbributed](#open-source-contributions) to by Mike.  Everything listed is an open source effort, the distinction is only his role as owner or contributor.
