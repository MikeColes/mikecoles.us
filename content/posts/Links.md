---
title: Links
date: 2018-12-26T20:56:14-05:00
lastmod: 2019-02-21T20:56:14-05:00
cover: "/img/default1.jpg"
draft: false
categories: ["links"]
tags: ["tech", "life"]
description: Lists of sites that I find myself continually sharing with others.
---
## IT
### Development
[Towards Data Science](https://towardsdatascience.com/)

[Code Mentor](https://www.codementor.io/community/)

[Software Mill](https://blog.softwaremill.com/)

[Hacker Noon](https://hackernoon.com/)

### Operations

### DevOps

### RSS 

### Twitter
[Samy Kamkar](https://twitter.com/samykamkar) has a knack for subverting technology and finding the security holes within.

### Podcasts
https://PacketPushers.net

## Life
### Affiliate Programs
#### Acorn
By default, Acorns wants to sniff your bank transactions to find out what you purchase in order to monetization this information in the name of 'round-up savings'. Curtail this by not giving them your banking information and make manual deposits in your retirement plan.
#### Robinhood
Robin allows you to trade stocks for free. I wish this service was around when I was younger. Acorns is good for general savings. Robinhood is for when you have a specific company you'd like to invest in.