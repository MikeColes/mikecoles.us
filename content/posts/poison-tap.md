---
title: "PoisonTap addition of DHCP route injection"
date: 2016-11-21T13:56:46-05:00
description: "Pushed routes via DHCP that would allow for deeper injection"
tags: ["poisontap","git","DHCP","Security"]
limage: "/img/poisontap.jpg"
link: "https://samy.pl/poisontap/"
fact: "Extended influence of PoisonTap to push deeper into the target device"
weight: 999
sitemap:
  priority : 0.5
featured: true
type: post
draft: false
---

[Samy Kamkar](https://samy.pl/) developed the new idea of idea of using a Rapberry Pi zero as an offensive device in security assessments. This contribution was a polishing of the injection of routes via DHCP

