{
"title": "First Post",
"date": "2018-12-26T20:27:50-05:00",
"lastmod": "2018-12-26T20:27:50-05:00",
"cover": "/images/default1.jpg",
"draft": false,
"categories": ["blog"],
"tags": ["blog", "tech"],
"description": "Intro to this area of the site.",
"weight": "50",
"sitemap": {"priority" : "0.8"},
"featured": "true"
}

First Post:

It begins. Again. Years ago, I ran blips.net and diyaday.com. They were mostly a place for me to fool around, share pictures, and figure out how techbology worked. Now, I'm back to using a website to figure out how technology works. The impetus for this site is to determine the best way to integrate CI/CD into my work flows.



Credits:

Static Site Generator	: Hugo

Theme 				: hugo-resume

Editor				: vi (and later VS Code)
