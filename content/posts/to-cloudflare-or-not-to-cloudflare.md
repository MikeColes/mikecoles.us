---
title: "To Cloudflare or Not to Cloudflare"
date: 2019-02-15T20:36:26-05:00
description: ""
draft: false
tags: ["CDN", "cloudflare"]
type: post
showTableOfContents: true
---



After seeing simple exploits making their way through Incapsula, Cloudflare earned a shot. For the price of 'free', they are offering DDOS and CDN. Thoughts of "If you're not paying for it, you're the product" went into overdrive. Combine this with the common grip of CloudFlare effectively performing a man-in-the-middle (MITM) attack and you have some healthy reasons to be skeptical. Both [Scott Helme](https://scotthelme.co.uk/tls-conundrum-and-leaving-cloudflare/) and [Troy Hunt](https://www.troyhunt.com/cloudflare-ssl-and-unhealthy-security-absolutism/) have both discussed the issues prior. Both have also decided Cloudflare is worth the risk.

5 minutes to create an account and change some NS records is all it takes to start using the service. Cloudflare certainly makes the onboarding easy. While waiting for TTLs to timeout, exploring of their website showed the available reports and features available. Reporting brought up another issue. If they are caching all, or most of, the traffic, the traffic monitors that already in place are about to go silent.

Even with the perceived pitfalls, Cloudflare is worth checking out. Expect a report in about a week.