---
title: "Mike Coles"
date: 2018-12-10T18:56:13-05:00
sitemap:
  priority : 1.0
---
Network whisperer and firewall finagler.

Mike has had the passion for systems since eight years of age. First online with CompuServe id 76022,455, he finds the idea of computer networks enthralling. 

Technology is a passion for Mike and he is always poking and probing new technology to see where it can be applied.

This site was generated using [HUGO](https://gohugo.io/), with templating from an [open source theme](https://github.com/eddiewebb/hugo-resume), which was then published via [GitLab](https://git.mikecoles.io/mike/mikecoles.us) and served by a [Caddy](https://caddyserver.com/) server after being deployed with CI/CD.

